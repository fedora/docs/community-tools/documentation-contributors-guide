= How to use GitLab UI for more involved document maintenance
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
:revdate: 2023-05-13

[abstract]
Documentation needs maintainance as the content grows. Writers and reviewers can contribute to Docs easily and efficiently inside GitLab Web User Interface. From Web IDE, CI pipeline to collaborative review of rendered web content and approval of pages, you can work on good content and document maintenance, uninterrupted by installation and configuration. The article assumes you are already familiar with Git and continuous integration (CI).

== Document maintenance

Maintenance of documentation takes many forms.

- Technical accuracy
- Up-to-dateness
- Curation of documentation in a logical order
- Consistency in presentation
- Standard template, attributes and conventions applied throughout Docs repos
- Use of CI pipeline to automate docs quality checks

The following sections step through how to maintain and continually improve quality of Fedora Docs repos using built-in tools in GitLab.

== GitLab Web IDE

Launched as part of the GitLab 15.7 release in December 2022, the new Web IDE provides a file explorer, a text editor and source control in one place.

=== Explorer

The explorer on the left pane helps to discover the repository structure and file list in Fedora Docs. Whichever repos, standard repository structure allows contributors to navigate files and cross-reference multiple pages quickly.

.Explorer
image::gitlab-ui/explorer-intro.png[]

=== Text editor

After making changes, go to the Source Control icon in the Activity Bar and click *Changes* under the *Commit & Push* button to view a list of files you changed in side-by-side view. If you made multiple commits, *Changes* indicate an overview of changes you have.

.View a list of changed files
image::gitlab-ui/source-control.png[]

If you click *Create MR*, you will be forwarded to your fork to create merge request. *Go to project* option is suitable when you made small commits by stage and want to squash them into one.

.Write, commit, create MR
image::gitlab-ui/commit.png[]

== CI pipeline

Automated test for Docs triggers syntax validation, stylistic errors, and helps fix them before MR is merged. The goals are project-wide consistency of documentation and adherence to the style guides. Docs team introduced a syntax-aware documentation linter for a few repos where you can find vale configuration files. Some contributors write articles without a knowledge of style guides and readability. Refer to the vale linter configuration for CI in the relevant repos:

- https://gitlab.com/fedora/docs/community-tools/documentation-contributors-guide/-/tree/main/.vale/styles/RedHat[Contributor Guides]

- https://gitlab.com/fedora/docs/docs-website/pages/-/tree/main/.vale/styles/RedHat[Docs Home Page]

Documentation linter carries out more than 20 tests:

- To trigger a CI pipeline to scan for any errors.
- To lint the words and structure of the documentation.
- To check the validity of links.
- To check the readability and run tests for conscious language and more.

Please be aware linter helps you write better, but it does not auto correct errors.

=== Visual reviews

With review apps, a live preview of rendered page is displayed if you click the *view app* icon or *view deployment* in preview MR_number.

.Live preview using view app
image::gitlab-ui/view-app.png[]

You will be presented with Artifacts-build page with job number and a link to the rendered page hosted on GitLab. Click the link to inspect the content just like you used to run Docsbuild script in local computer.

.MR preview in deployment
image::gitlab-ui/preview.png[]

Previewing changes during MR review facilitates close collaboration to catch errors and make suggestions to improve content.

The *View app* button disappears after MR is merged.

=== Code quality report

To view result of CI linting, go to Pipelines on the left pane and click *code quality*.

.View result of CI linting
image::gitlab-ui/pipeline.png[]

Docs team will evaluate options to reflect changes systematically based on code quality report.

Thanks for your contributions.
